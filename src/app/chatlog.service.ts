import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatlogService {
  getLog() {
    return this.httpClient.get('assets/chatlog.html', { responseType: 'text' })
  }
  constructor(private httpClient: HttpClient) { }
}
