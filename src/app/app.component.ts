import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ChatlogService } from './chatlog.service';


interface CharacterStats {
  name: string,
  occurances: number
  totalDamageDealt: number,
  averageDamaageDealt: number,
  totalHealingDealt: number, 
  averageHealingDealt: number,
  totalDamageReceived: number,
  averageDamageReceived: number,
  totalHealingReceived: number, 
  averageHealingReceived: number,
}

interface Session {
  date: string,
  startLine: number, 
  endLine: number,
  characters: CharacterStats[]
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'fg-chat-parser';

  fileReader = new FileReader

  chatlog?: string

  dates: string[] = []

  sessions: Session[] = []

  activeSession: Session = this.sessions[0]

  activeChar: CharacterStats = {
    name: '',
    occurances: 0,
    totalDamageDealt: 0,
    averageDamaageDealt: 0,
    totalHealingDealt: 0,
    averageHealingDealt: 0,
    totalDamageReceived: 0,
    averageDamageReceived: 0,
    totalHealingReceived: 0,
    averageHealingReceived: 0
  }

  uploadForm = new FormGroup({
    file: new FormControl('',[Validators.required]),
    fileSource: new FormControl()
  })


  onSubmit(){
    console.log(this.uploadForm.value);
    this.fileReader.readAsText(this.uploadForm.value.fileSource)
    this.fileReader.onload = () => {
      if (typeof this.fileReader.result === 'string') {
        const fileContent: string = this.fileReader.result.toString();
        // do something with fileContent, e.g. parse it
        this.chatlog=fileContent
        this.parseLog(fileContent)
      }
    };
  }  

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  fileChanged(event: any){
    console.log(event);
    
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.patchValue({
        fileSource: file
      });
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  changeSession(event: any) {
    console.log(event.target.value);
    this.activeSession = this.sessions[this.dates.indexOf(event.target.value)]
  }

  parseLog(chatlog:string){
    const arr = chatlog.split('\n');
    let index = 0
    arr.forEach(element => {
      index++
      const match = element.match(/name="(\d{4}-\d{2}-\d{2})"/)
      // If the next line represents the start of a new session
      if (match) {
        // If the line is part of an existing session
        if (this.dates.includes(match[1])) {
          this.sessions[this.sessions.length-1].endLine=index
        }
        // The line is part of a new session
        else{
          this.sessions.push({date: match[1],startLine: index, endLine: index, characters: []})
          this.dates.push(match[1])
        }
      } else {
        // The line is part of an ongoing session
        if (this.sessions.length == 0) {
          console.log('first line');
        } else {
          // Set the current session
          const curSession = this.sessions[this.sessions.length - 1]
          // Set the current list of characters
          const curCharacters = curSession.characters
          // Increment the index of the last line
          curSession.endLine = index
          // Analyse the line for any new data.

          // if the line contains a character
          const character = element.match(/\[TURN\]\s(.+?)<\/font>/)
          if (character) {
            if (curCharacters) {
              const checkingCharacter = curCharacters.find(obj => 
                obj.name === character[1].toString()
              )
              if (!checkingCharacter) {
                const newChar: CharacterStats = {
                  name: character[1].toString(),
                  occurances: 1,
                  totalDamageDealt: 0,
                  averageDamaageDealt: 0,
                  totalHealingDealt: 0,
                  averageHealingDealt: 0,
                  totalDamageReceived: 0,
                  averageDamageReceived: 0,
                  totalHealingReceived: 0,
                  averageHealingReceived: 0
                }
                curCharacters.push(newChar)
                this.activeChar = newChar
              } else {
                checkingCharacter.occurances++
                // Check what action they did and increment the appropriate stat
                this.activeChar = checkingCharacter
              }
            }
          }

          // If the line contains an attack
          const attack = element.match(/Attack \[([0-9]+)\]/)
          if (attack) {
            const target = String(attack.input?.match(/\[at\s(.+?)\]/))
            console.log(`${this.activeChar?.name} attacked ${target.split(',')[1]} with a ${attack[1]}`);
          }
          // If the line contains damage
          const damage = element.match(/Damage \[([0-9]+)\]/)
          if (damage) {
            const target = String(damage.input?.match(/\[to\s(.+?)\]/))
            console.log(`${this.activeChar?.name} dealt ${damage[1]} damage to ${target.split(',')[1]}`);
            // Increase the active characters damage dealt
            if (this.activeChar) {
              this.activeChar.totalDamageDealt += parseInt(damage[1])
            }
            
            // Check if the damaged character exists in the tracker yet.
            const checkingCharacter = curCharacters.find(obj => 
                obj.name === target.split(',')[1].toString()
              )
            if (!checkingCharacter) {
              const newChar: CharacterStats = {
                name: target.split(',')[1].toString(),
                occurances: 1,
                totalDamageDealt: 0,
                averageDamaageDealt: 0,
                totalHealingDealt: 0,
                averageHealingDealt: 0,
                totalDamageReceived: parseInt(damage[1]),
                averageDamageReceived: 0,
                totalHealingReceived: 0,
                averageHealingReceived: 0
              }
              curCharacters.push(newChar)
            } else {
              checkingCharacter.occurances++
              checkingCharacter.totalDamageReceived += parseInt(damage[1])
            }
          } 

          // If the line contains a heal attempted
          const giveHeal = element.match(/<font.*?>(.*?): \[(HEAL)\] .*?\[.*? = (\d+)\]<br \/>/)
          if (giveHeal) {
            const checkingCharacter = curCharacters.find(obj => 
                obj.name === giveHeal[1].toString()
              )
              if (!checkingCharacter) {
                const newChar: CharacterStats = {
                  name: giveHeal[1].toString(),
                  occurances: 1,
                  totalDamageDealt: 0,
                  averageDamaageDealt: 0,
                  totalHealingDealt: parseInt(giveHeal[3]),
                  averageHealingDealt: 0,
                  totalDamageReceived: 0,
                  averageDamageReceived: 0,
                  totalHealingReceived: 0,
                  averageHealingReceived: 0
                }
                curCharacters.push(newChar)
              } else {
                checkingCharacter.occurances++
                // Check what action they did and increment the appropriate stat
                checkingCharacter.totalHealingDealt += parseInt(giveHeal[3]) 
              }
          }

          // If the line contains a heal done
          const heal = element.match(/Heal \[([0-9]+)\]/)
          if (heal) {
            const target = String(heal.input?.match(/\[to\s(.+?)\]/))
            console.log(`${target.split(',')[1]} healed ${heal[1]} points`);
            
            // Check if the healed character exists in the tracker yet.
            const checkingCharacter = curCharacters.find(obj => 
                obj.name === target.split(',')[1].toString()
              )
            if (!checkingCharacter) {
              const newChar: CharacterStats = {
                name: target.split(',')[1].toString(),
                occurances: 1,
                totalDamageDealt: 0,
                averageDamaageDealt: 0,
                totalHealingDealt: 0,
                averageHealingDealt: 0,
                totalDamageReceived: 0,
                averageDamageReceived: 0,
                totalHealingReceived: parseInt(heal[1]),
                averageHealingReceived: 0
              }
              curCharacters.push(newChar)
            } else {
              checkingCharacter.occurances++
              checkingCharacter.totalHealingReceived += parseInt(heal[1])
            }
          }
        }
      }
    });
    console.log(this.sessions);
  }

  constructor(private clService: ChatlogService) {
    //this.sessions.push
  }

  ngOnInit(): void {
    this.clService.getLog().subscribe(
      value => {
        this.parseLog(value)
      }
    )
  }
}
